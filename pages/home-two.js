import React from 'react'
import { Preloader, Placeholder } from 'react-preloading-screen'
import NoSSR from 'react-no-ssr'
import Header from '../components/Layouts/Header'
import MainBanner from '../components/home-two/MainBanner'
import ServicesArea from '../components/provider/ServicesArea'
import Team from '../components/provider/Team'
import Funfacts from '../components/provider/Funfacts'
import RecentWork from '../components/provider/RecentWork'
import Pricing from '../components/provider/Pricing'
import Feedback from '../components/provider/Feedback'
import Partner from '../components/provider/Partner'
import Blog from '../components/provider/Blog'
import Footer from '../components/Layouts/Footer'
import GoTop from '../components/Layouts/GoTop'

export default () => (
    <NoSSR>
        <Preloader>
            <Placeholder>
                <div className="preloader">
                    <div className="spinner"></div>
                </div>
            </Placeholder>
            <Header />
            <MainBanner />
            <ServicesArea />
            <Team />
            <Funfacts />
            <RecentWork />
            <Pricing />
            <Feedback />
            <Partner />
            <Blog />
            <Footer />
            <GoTop scrollStepInPx="50" delayInMs="16.66" />
        </Preloader>
    </NoSSR>
)
