import App, { Container } from 'next/app'
import Head from 'next/head'
import React from 'react'
import { trackPageView } from "../googleAnalitics";
import { Router } from 'next/router'

export default class MyApp extends App {
    componentDidMount() {
        Router.onRouteChangeComplete = url => {
            trackPageView(url);
        };
    }

    static async getInitialProps({ Component, router, ctx }) {
        let pageProps = {};

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }

        return { pageProps }
    }

    render() {
        const { Component, pageProps } = this.props;

        return (
            <>
                <Head>
                    <title>Work Favour</title>
                    <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet"
                        type="text/css" />
                    <meta name="Description" content="Платформа за студенти и набиране на персонал за почасова работа" />
                </Head>

                <Component {...pageProps} />
            </>
        )
    }
}
