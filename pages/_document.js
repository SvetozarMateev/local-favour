import Document, { Html, Head, Main, NextScript } from 'next/document'
import React from "react";

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        // Check if in production
        const isProduction = process.env.NODE_ENV === 'production';
        const initialProps = await Document.getInitialProps(ctx);
        // Pass isProduction flag back through props
        return { ...initialProps, isProduction };
    }

    setGoogleTags() {
        return {
            __html: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-146414966-1');
      `
        };
    }

    render() {
        return (
            <Html lang="en">
                <Head>
                    {/* <meta charSet="utf-8" /> */}
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                    <link rel="icon" type="image/png" href={require("../assets/img/teaser/logo_WF_symbol.png?webp")}></link>
                    <meta name="Description" content="Платформа за студенти и набиране на персонал за почасова работа" />
                </Head>
                <body>
                    <Main />
                    <NextScript />

                    {this.props.isProduction && (
                        <>
                            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146414966-1"></script>

                            {/* We call the function above to inject the contents of the script tag */}
                            <script dangerouslySetInnerHTML={this.setGoogleTags()} />
                        </>
                    )}
                </body>
            </Html>
        )
    }
}

export default MyDocument
