import React, { useState } from 'react';
import NoSSR from "react-no-ssr";
import { Placeholder, Preloader } from "react-preloading-screen";
import Loader from "../components/Loader";
import FeedbackDialog from "../components/provider/Dialog";
import Header from "../components/Layouts/Header";
import MainBanner from "../components/provider/MainBanner";
import OurFeatures from "../components/provider/OurFeatures";
import Footer from "../components/Layouts/Footer";
import GoTop from "../components/Layouts/GoTop";
import ParticlesMy from '../components/provider/ParticlesMy';

export const config = { amp: "hybrid" }

const Provider = () => {
    const [open, setOpen] = useState(false);

    function openFeedback() {
        setOpen(true);
    }

    function closeFeedback() {
        setOpen(false);
    }

    //
    // let options = {
    //     throttle: 100,
    // };
    //
    // let position = useWindowScrollPosition(options);
    // console.log("position", position);

    // setTimeout(() => {
    //     openFeedback();
    // }, 15000);

    return (
        <NoSSR>
            <Preloader>
                <Placeholder>
                    <Loader />
                </Placeholder>

                <ParticlesMy />

                <FeedbackDialog closeFeedback={closeFeedback}
                    open={open || false} />

                <Header
                    openFeedback={openFeedback}
                />

                <MainBanner />

                <OurFeatures />

                <div>
                    <div className="coming-soon-content" id="daiEmail">
                        <label htmlFor="MERGE0">
                            <h4 style={{
                                color: "#6838A0"
                            }}>Намерете персонал през WorkFavour </h4>
                            <p style={{ color: "#6084a4" }}>
                                Запишете имейл адреса Ви и ние ще се свържем с вас!
                            </p>
                        </label>
                        <form id="mce-EMAIL" action="https://workfavour.us3.list-manage.com/subscribe/post" method="POST">
                            <input type="hidden" name="u" value="c7fe55f1dff1e3c76e8cd7e33" />
                            <input type="hidden" name="id" value="82d242f5ad" />

                            <input type="email" className="email-input"
                                placeholder="Вашият Email"
                                autoCapitalize="off" autoCorrect="off" name="MERGE0" id="MERGE0" />


                            <div className="submit_container clear">
                                <input type="submit" name="submit" value="Абониране" className="submit-btn" />
                            </div>

                        </form>
                    </div>
                </div>

                {/*<ServicesArea/>*/}

                <Footer />
                <GoTop scrollStepInPx="50" delayInMs="16.66" />
            </Preloader>
        </NoSSR>
    )
};

export default Provider;
