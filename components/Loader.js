import React from 'react';
import createReactClass from 'create-react-class';
import '../assets/css/loader.css';

const Loader = createReactClass({
    render() {
        return (
            <div className="loader">
                <div className="side"></div>
                <div className="side"></div>
                <div className="side"></div>
                <div className="side"></div>
                <div className="side"></div>
                <div className="side"></div>
                <div className="side"></div>
                <div className="side"></div>
            </div>
        );
    }
});

export default Loader;
