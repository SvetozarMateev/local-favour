import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../assets/css/style.css'
import '../../assets/css/slick.css'
import '../../assets/css/responsive.css'
import '../../assets/css/animate.css'
import React from 'react'
import Link from '../common/ActiveLink'
import * as Icon from 'react-feather';
import logo from "../../assets/img/teaser/logo_WF@2x.png"
import '../../assets/css/custom.css'
import { withRouter } from "next/router";

class DefaultStyle extends React.Component {

    state = {
        collapsed: true,
    };

    toggleNavbar = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    componentDidMount() {
        let elementId = document.getElementById("navbar");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId.classList.add("is-sticky");
            } else {
                elementId.classList.remove("is-sticky");
            }
        });
        window.scrollTo(0, 0);
    }

    render() {
        const { collapsed } = this.state;
        //console.log("OPAAA", this.props.router.pathname)
        const classOne = 'collapse navbar-collapse show';
        const classTwo = collapsed ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

        const btnStyle = {
            backgroundColor: "#FFFFFF",
            color: "#6B2FA2"
        };
        return (
            <header id="header">
                <div id="navbar" className="startp-nav p-relative">
                    <div className="container">
                        <nav className="navbar navbar-expand-md navbar-light">
                            <Link href="/">
                                <a className="navbar-brand">
                                    <img src={logo} alt="logo" id="logo" />
                                </a>
                            </Link>

                            {/*<button*/}
                            {/*    onClick={this.toggleNavbar}*/}
                            {/*    className={classTwo}*/}
                            {/*    type="button"*/}
                            {/*    data-toggle="collapse"*/}
                            {/*    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"*/}
                            {/*    aria-expanded="false"*/}
                            {/*    aria-label="Toggle navigation"*/}
                            {/*>*/}
                            {/*    <span className="navbar-toggler-icon"></span>*/}
                            {/*</button>*/}

                            <div className={classOne} id="navbarSupportedContent" style={{
                                flexDirection: "row-reverse",
                                flexWrap: "wrap-reverse"
                            }}>
                                {/*<Link href="#">*/}
                                {/*    <a className="btn btn-light" onClick={this.props.openFeedback}>Join</a>*/}
                                {/*</Link>*/}
                                <Link href="/" >
                                    <a className={"btn btn-primary btn" + `${!this.props.router.pathname.includes("provider") ? " disabled" : ""}`}>Търся работа</a>
                                </Link>
                                <Link href="/provider">
                                    <a className={"btn btn-primary btn" + `${this.props.router.pathname.includes("provider") ? " disabled" : ""}`}>Търся персонал</a>
                                </Link>

                                {/* {this.props.router.pathname.includes("provider")
                                    ? <Link href="/">
                                        <a className="btn btn-primary">Търся Работа</a>
                                    </Link>
                                    : <Link href="/provider">
                                        <a className="btn btn-primary">Търся Персонал</a>
                                    </Link>} */}
                            </div>

                            {/*<div className="others-option">*/}
                            {/*    /!*<Link href="#">*!/*/}
                            {/*    /!*    <a className="btn btn-light" onClick={this.props.openFeedback}>Join</a>*!/*/}
                            {/*    /!*</Link>*!/*/}
                            {/*    <Link href="#">*/}
                            {/*        <a className="btn btn-primary">Търся Работа</a>*/}
                            {/*    </Link>*/}
                            {/*</div>*/}
                        </nav>
                    </div>
                </div>

            </header >
        )
    }
}

export default withRouter(DefaultStyle);
