import React from 'react'
import Link from 'next/link'
import * as Icon from 'react-feather'
import logo from "../../assets/img/teaser/logo_WF@2x.png?webp";

export default class Footer extends React.Component {
    render() {
        return (
            <footer className="footer-area bg-f7fafd">
                <div style={{
                    paddingLeft: 30
                }} className="container">
                    <div className="row">
                        <div className="col-lg-5 col-md-6">
                            <div className="single-footer-widget">
                                <div className="logo">
                                    <Link href="/">
                                        <a className="navbar-brand">
                                            <img src={logo} alt="logo" id="logo" />
                                        </a>
                                    </Link>
                                </div>
                                <p>
                                    Първият български онлайн посредник за намиране и предлагане на почасова работа.
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="single-footer-widget">
                                <h3>Намере ни: </h3>

                                <ul className="social-links">
                                    <li>
                                        <a href="https://www.facebook.com/WorkFavour/" target="_blank" className="facebook">
                                            <Icon.Facebook />
                                        </a>
                                    </li>
                                    <li>
                                        <Link href="#">
                                            <a target="_blank" className="twitter"><Icon.Twitter /></a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="#">
                                            <a target="_blank" className="instagram"><Icon.Instagram /></a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="#">
                                            <a target="_blank" className="linkedin"><Icon.Linkedin /></a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div className="single-footer-widget">
                                <h3>Контакти</h3>

                                <ul className="footer-contact-info">
                                    <li>
                                        <Icon.Mail />
                                        Email: <a>team@workfavour.com</a>
                                    </li>
                                    <li>
                                        <Icon.PhoneCall />
                                        Phone: <a>+ (359) 8888 555 30</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <img style={{
                    width: "100%",
                    height: "100%"
                }}
                    src={require("../../images/map.png")} className="map" alt="map" />

            </footer>
        )
    }
}
