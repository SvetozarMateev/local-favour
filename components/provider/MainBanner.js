import React from 'react'
import travel from "../../assets/img/teaser/mainimage@2x.png?webp";

const MainBanner = () => {
    return (
        <div className="main-banner" ref={(el) => el = el} >
            <div className="d-table" >
                <div className="d-table-cell" id="main-banner-inner">
                    <div className="container">
                        <div className="row h-100 justify-content-center align-items-center mainPanel">
                            <div className="col-lg-5">
                                <div className="hero-content">
                                    <h1>Нуждаете се от персонал за временна работа ? </h1>
                                    <p style={{
                                        fontSize: 19
                                    }}>WorkFavour позволява бърз, лесен и ефикасен подбор на кадри, предложени според нуждите Ви</p>
                                    {/*<FeedbackDialog/>*/}

                                </div>
                            </div>
                            {/*<img src={blueImg} alt=""/>*/}
                            <div className="col-lg-6 col-md-12 services-left-image">
                                <img style={{ top: "-210px", maxWidth: "85%" }}
                                    src={travel}
                                    alt="img" />
                                <div></div>
                                {/*<FancyImage1/>*/}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            {/*<div className="shape1">*/}
            {/*    <img src={require("../../images/shape1.png")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape2 rotateme">*/}
            {/*    <img src={require("../../images/shape2.svg")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape3">*/}
            {/*    <img src={require("../../images/shape3.svg")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape4">*/}
            {/*    <img src={require("../../images/shape4.svg")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape5">*/}
            {/*    <img src={require("../../images/shape5.png")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape6 rotateme">*/}
            {/*    <img src={require("../../images/shape4.svg")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape7">*/}
            {/*    <img src={require("../../images/shape4.svg")} alt="shape"/>*/}
            {/*</div>*/}
            {/*<div className="shape8 rotateme">*/}
            {/*    <img src={require("../../images/shape2.svg")} alt="shape"/>*/}
            {/*</div>*/}
        </div>
    )
}

export default MainBanner
