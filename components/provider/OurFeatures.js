import React from 'react'
import icon1 from '../../assets/img/teaser/icon1_color@2x.png?webp';
import icon2 from '../../assets/img/teaser/icon2_color@2x.png?webp';
import icon3 from '../../assets/img/teaser/icon3_color@2x.png?webp';
import icon4 from '../../assets/img/teaser/icon4_color@2x.png?webp';

const OurFeatures = (props) => {
	return (
		<section className="boxes-area">
			<div className="container">
				<div className="row">
					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon1} alt="" />
							</div>
							<h3>Гарантирано качество на персонала</h3>
							<p>Намерете персонал за временна работа бързо и лесно, с гаранцията, че кандидатът ще отговаря на изискванията Ви.</p>
						</div>
					</div>

					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon2} alt="" />
							</div>
							<h3>Избор на кадри</h3>
							<p>Разполагайте с възможността да избирате кандидати от богатата ни селекция талантливи и квалифицирани кадри.</p>
						</div>
					</div>

					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon3} alt="" />
							</div>
							<h3>Бързо и лесно</h3>
							<p>Лесният за употреба интерфейс Ви позволява да предлагате бързо работни смени, започващи и завършващи, когато прецените за нужно.</p>
						</div>
					</div>

					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon4} alt="" />
							</div>
							<h3>По-малко бюрокрация</h3>
							<p>WF ви предоставя възможността да използвате предварително изготвени от нашият екип договори за услуга при наемане на персонал.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}

export default OurFeatures
