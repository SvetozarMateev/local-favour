import Particles from 'react-particles-js';
import React from 'react';
import createReactClass from 'create-react-class';
import person from "../../assets/img/mand.svg"
import config from '../../particlesjs-config'


const ParticlesMy = createReactClass({
    render() {
        return (
            <Particles
                style={{position: "absolute"}}
                params={config}
                // params={{
                //     "particles": {
                //         "number": {
                //             "value": 110,
                //             "density": {
                //                 "enable": true,
                //                 "value_area": 1183.721462448409
                //             }
                //         },
                //         "color": {
                //             "value": "#8c8c8c"
                //         },
                //         "shape": {
                //             "type": "image",
                //             "image": {
                //                 "src": person,
                //                 "width": 100,
                //                 "height": 100
                //             }
                //         },
                //         "opacity": {
                //             "value": 0.5,
                //             "random": false,
                //             "anim": {
                //                 "enable": false,
                //                 "speed": 1,
                //                 "opacity_min": 0.1,
                //                 "sync": false
                //             }
                //         },
                //         "size": {
                //             "value": 4,
                //             "random": true,
                //             "anim": {
                //                 "enable": false,
                //                 "speed": 40,
                //                 "size_min": 0.1,
                //                 "sync": false
                //             }
                //         },
                //         "line_linked": {
                //             "enable": true,
                //             "distance": 200,
                //             "color": "#2f2f2f",
                //             "opacity": 0.4,
                //             "width": 1
                //         },
                //         "move": {
                //             "enable": false,
                //             "speed": 2.5,
                //             "direction": "none",
                //             "random": false,
                //             "straight": false,
                //             "out_mode": "out",
                //             "bounce": false,
                //             "attract": {
                //                 "enable": false,
                //                 "rotateX": 600,
                //                 "rotateY": 1200
                //             }
                //         }
                //     },
                //     "interactivity": {
                //         "detect_on": "window",
                //         "events": {
                //             "onhover": {
                //                 "enable": true,
                //                 "mode": "grab"
                //             },
                //             "onclick": {
                //                 "enable": true,
                //                 "mode": "repulse"
                //             },
                //             "resize": true
                //         },
                //         "modes": {
                //             "grab": {
                //                 "distance": 200,
                //                 "line_linked": {
                //                     "opacity": 1
                //                 }
                //             },
                //             "bubble": {
                //                 "distance": 400,
                //                 "size": 40,
                //                 "duration": 2,
                //                 "opacity": 8,
                //                 "speed": 3
                //             },
                //             "repulse": {
                //                 "distance": 200,
                //                 "duration": 0.4
                //             },
                //             "push": {
                //                 "particles_nb": 7
                //             },
                //             "remove": {
                //                 "particles_nb": 2
                //             }
                //         }
                //     },
                // }}
            >
                {this.props.children}

            </Particles>
        );
    }
});

export default ParticlesMy;
