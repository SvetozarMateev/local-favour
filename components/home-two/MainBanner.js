import React from 'react'
import OwlCarousel from 'react-owl-carousel3'
import * as Icon from 'react-feather'
import Link from 'next/link';
import ParticlesMy from "../provider/ParticlesMy";

const options = {
    items: 4,
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    responsive: {
        0: {
            items: 1
        },
        786: {
            items: 2
        },
        1200: {
            items: 4
        }
    }
}

export default class MainBanner extends React.Component {
    onFormSubmit = (e) => {
        e.preventDefault();
    };

    render() {
        return (
            <React.Fragment>
                <div className="main-banner">
                    <ParticlesMy></ParticlesMy>

                    <div className="d-table">
                        <div className="d-table-cell">
                            <div className="container">
                                <div className="row h-100 justify-content-center align-items-center">
                                    <div className="col-lg-5">
                                        <div className="hero-content">
                                            <h1>Персонал за вашия бизнес</h1>
                                            <p>WorkFavour позволява бърз, лесен и ефикасен подбор на кадри, предложени според нуждите Ви.</p>
                                            <Link href="#">
                                                <a className="btn btn-primary">Get Started</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="col-lg-5 offset-lg-1">
                                        <div className="banner-form ml-3">
                                            <form onSubmit={this.onFormSubmit}>
                                                <div className="form-group">
                                                    <label>Username</label>
                                                    <input type="text" className="form-control"
                                                           placeholder="Enter username"/>
                                                </div>

                                                <div className="form-group">
                                                    <label>Email</label>
                                                    <input type="email" className="form-control"
                                                           placeholder="Enter your email"/>
                                                </div>

                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <input type="password" className="form-control"
                                                           placeholder="Create a password"/>
                                                </div>

                                                <button type="submit" className="btn btn-primary">Register Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="shape1"><img src={require("../../images/shape1.png")} alt="shape"/></div>
                    <div className="shape2 rotateme"><img src={require("../../images/shape2.svg")} alt="shape"/></div>
                    <div className="shape3"><img src={require("../../images/shape3.svg")} alt="shape"/></div>
                    <div className="shape4"><img src={require("../../images/shape4.svg")} alt="shape"/></div>
                    <div className="shape5"><img src={require("../../images/shape5.png")} alt="shape"/></div>
                    <div className="shape6 rotateme"><img src={require("../../images/shape4.svg")} alt="shape"/></div>
                    <div className="shape7"><img src={require("../../images/shape4.svg")} alt="shape"/></div>
                    <div className="shape8 rotateme"><img src={require("../../images/shape2.svg")} alt="shape"/></div>
                </div>


            </React.Fragment>
        )
    }
}

