import React from 'react';
import createReactClass from 'create-react-class';
import ReactWOW from "react-wow";

const OurMission = createReactClass({
    render() {
        return (
            <div className="container">
                <div className="row h-100 justify-content-center align-items-center">
                    <div className="col-lg-6 col-md-12 services-left-image">
                        <ReactWOW delay='0.6s' animation='fadeInDown'>
                            <img
                                src={require("../../images/services-left-image/big-monitor.png")}
                                className="wow fadeInDown"
                                data-wow-delay="0.6s"
                                alt="big-monitor"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='fadeInUp'>
                            <img
                                src={require("../../images/services-left-image/creative.png")}
                                className="wow fadeInUp"
                                data-wow-delay="0.6s"
                                alt="creative"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='fadeInLeft'>
                            <img
                                src={require("../../images/services-left-image/developer.png")}
                                className="wow fadeInLeft"
                                data-wow-delay="0.6s"
                                alt="developer"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='zoomIn'>
                            <img
                                src={require("../../images/services-left-image/flower-top.png")}
                                className="wow zoomIn"
                                data-wow-delay="0.6s"
                                alt="flower-top"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='bounceIn'>
                            <img
                                src={require("../../images/services-left-image/small-monitor.png")}
                                className="wow bounceIn"
                                data-wow-delay="0.6s"
                                alt="small-monitor"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='fadeInDown'>
                            <img
                                src={require("../../images/services-left-image/small-top.png")}
                                className="wow fadeInDown"
                                data-wow-delay="0.6s"
                                alt="small-top"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='zoomIn'>
                            <img
                                src={require("../../images/services-left-image/table.png")}
                                className="wow zoomIn"
                                data-wow-delay="0.6s"
                                alt="table"
                            />
                        </ReactWOW>
                        <ReactWOW delay='0.6s' animation='fadeInUp'>
                            <img
                                src={require("../../images/services-left-image/target.png")}
                                className="wow fadeInUp"
                                data-wow-delay="0.6s"
                                alt="target"
                            />
                        </ReactWOW>

                        <img
                            src={require("../../images/services-left-image/cercle-shape.png")}
                            className="bg-image rotateme"
                            alt="shape"
                        />
                        <ReactWOW delay='0.6s' animation='fadeInUp'>
                            <img
                                src={require("../../images/services-left-image/main-pic.png")}
                                className="wow fadeInUp"
                                data-wow-delay="0.6s"
                                alt="main-pic"
                            />
                        </ReactWOW>
                    </div>

                    <div className="col-lg-6 col-md-12">
                        <div className="about-content">
                            <div className="section-title">
                                <h2>About Us</h2>
                                <div className="bar"></div>
                                <p>Lorem ipsum dolor sit amet, con se ctetur adipiscing elit. In sagittis eg esta
                                    ante, sed viverra nunc tinci dunt nec elei fend et tiram.</p>
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sagittis egestas ante,
                                sed viverra nunc tincidunt nec nteger nonsed condimntum elit, sit amet feugiat
                                lorem. Proin tempus sagittis velit vitae scelerisque.</p>

                            <p>Lorem ipsum dolor sit amet, con se ctetur adipiscing elit. In sagittis eg esta ante,
                                sed viverra nunc tinci dunt nec elei fend et tiram.</p>

                            <p>Business-to-business metrics analytics value proposition funding angel investor
                                entrepreneur alpha ramen equity gamification. Social proof partner network
                                research.</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

export default OurMission;
