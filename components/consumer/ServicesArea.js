import React from 'react'
import * as Icon from 'react-feather';
import OurMission from "./OurMission";

class ServicesArea extends React.Component {
    render() {
        return (
            <React.Fragment>
                <section className="features-area ptb-80">
                    <div className="container">
                        <div className="section-title">
                            <h2>Our Features</h2>
                            <div className="bar"></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>

                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <div className="single-features">
                                    <div className="icon">
                                        <Icon.Settings />
                                    </div>

                                    <h3>Incredible Infrastructure</h3>
                                    <p>Lorem ipsum dolor amet, adipiscing, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div className="single-features">
                                    <div className="icon">
                                        <Icon.Mail />
                                    </div>

                                    <h3>Email Notifications</h3>
                                    <p>Lorem ipsum dolor amet, adipiscing, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div className="single-features">
                                    <div className="icon bg-c679e3">
                                        <Icon.Grid />
                                    </div>

                                    <h3>Simple Dashboard</h3>
                                    <p>Lorem ipsum dolor amet, adipiscing, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div className="single-features">
                                    <div className="icon bg-c679e3">
                                        <Icon.Info />
                                    </div>

                                    <h3>Information Retrieval</h3>
                                    <p>Lorem ipsum dolor amet, adipiscing, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div className="single-features">
                                    <div className="icon bg-eb6b3d">
                                        <Icon.Box />
                                    </div>

                                    <h3>Drag and Drop Functionality</h3>
                                    <p>Lorem ipsum dolor amet, adipiscing, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div className="single-features">
                                    <div className="icon bg-eb6b3d">
                                        <Icon.Bell />
                                    </div>

                                    <h3>Deadline Reminders</h3>
                                    <p>Lorem ipsum dolor amet, adipiscing, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                {/*<section className="services-area ptb-80 bg-f7fafd">*/}
                {/*    <OurMission/>*/}
                {/*</section>*/}
            </React.Fragment>
        )
    }
}

export default ServicesArea
