import React, {Component} from 'react';
import Link from 'next/link'
import ReactWOW from 'react-wow'

class FancyImage1 extends Component {
    render() {
        return (
            <>
                <ReactWOW delay='0.6s' animation='fadeInDown'>
                    <img
                        src={require("../../images/services-left-image/big-monitor.png")}
                        className="wow fadeInDown"
                        data-wow-delay="0.6s"
                        alt="big-monitor"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='fadeInUp'>
                    <img
                        src={require("../../images/services-left-image/creative.png")}
                        className="wow fadeInUp"
                        data-wow-delay="0.6s"
                        alt="creative"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='fadeInLeft'>
                    <img
                        src={require("../../images/services-left-image/developer.png")}
                        className="wow fadeInLeft"
                        data-wow-delay="0.6s"
                        alt="developer"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='zoomIn'>
                    <img
                        src={require("../../images/services-left-image/flower-top.png")}
                        className="wow zoomIn"
                        data-wow-delay="0.6s"
                        alt="flower-top"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='bounceIn'>
                    <img
                        src={require("../../images/services-left-image/small-monitor.png")}
                        className="wow bounceIn"
                        data-wow-delay="0.6s"
                        alt="small-monitor"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='fadeInDown'>
                    <img
                        src={require("../../images/services-left-image/small-top.png")}
                        className="wow fadeInDown"
                        data-wow-delay="0.6s"
                        alt="small-top"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='zoomIn'>
                    <img
                        src={require("../../images/services-left-image/table.png")}
                        className="wow zoomIn"
                        data-wow-delay="0.6s"
                        alt="table"
                    />
                </ReactWOW>
                <ReactWOW delay='0.6s' animation='fadeInUp'>
                    <img
                        src={require("../../images/services-left-image/target.png")}
                        className="wow fadeInUp"
                        data-wow-delay="0.6s"
                        alt="target"
                    />
                </ReactWOW>

                <ReactWOW delay='0.6s' animation='fadeInUp'>
                    <img
                        src={require("../../images/services-left-image/main-pic.png")}
                        className="wow fadeInUp"
                        data-wow-delay="0.6s"
                        alt="main-pic"
                    />
                </ReactWOW>
            </>
        );
    }
}

export default FancyImage1;