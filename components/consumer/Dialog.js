import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ComingSoon from "../../pages/coming-soon";
import withStyles from "@material-ui/core/styles/withStyles";
import robot from '../../assets/img/robot-3.png'
import Link from "@material-ui/core/Link";


const styles = {
    dialogPaper: {
        minHeight: '80vh',
        maxHeight: '80vh',
    },
};

function FeedbackDialog(props) {
    const { open, closeFeedback, classes } = props;

    const openAnketa = () => {

        closeFeedback();
    };

    return (
        <div>
            <Dialog open={open || false}
                classes={{ paper: classes.dialogPaper }}
                onClose={closeFeedback}
            >
                <DialogContent>
                    <div style={{ backgroundColor: "#1A0142" }}>
                        <img src={robot} alt="" />
                    </div>
                    <p>

                        Lorem ipsum dolor sit amet, placerat dui metus suspendisse iaculis, tempus urna ut non, morbi id donec eros.nostra pretium proin, elit porta at massa eu pellentesque gravida. Scelerisque porttitor at.
                    </p>
                    <div className="feedbackButton">
                        <Link href="#">
                            <a className="btn btn-primary" onClick={openAnketa}>Кратка Анкета</a>
                        </Link>
                    </div>

                    <Button variant="contained" color="secondary" onClick={closeFeedback}>
                        Не искам да участвам
                    </Button>
                </DialogContent>
            </Dialog>
        </div>
    );
}

export default withStyles(styles)(FeedbackDialog);