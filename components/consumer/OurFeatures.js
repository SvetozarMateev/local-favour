import React from 'react'
import icon1 from "../../assets/img/teaser/1.png?webp";
import icon2 from "../../assets/img/teaser/2.png?webp";
import icon3 from "../../assets/img/teaser/3.png?webp";
import icon4 from "../../assets/img/teaser/4.png?webp";


const OurFeatures = (props) => {
	return (
		<section className="boxes-area">
			<div className="container">
				<div className="row">
					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon1} alt="No long term contracts icon" />
							</div>
							<h3>Без дългосрочни договори</h3>
							<p>Разполагайте със свободата да работите, когато искате, без да се обвързвате с дългосрочни договори.</p>
						</div>
					</div>

					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon2} alt="Wage icon" />
							</div>
							<h3>Справедливо заплащане </h3>
							<p>Част от нашата мисия е да Ви осигурим заплащане, отговарящо на стойността на вашият труд.</p>
						</div>
					</div>

					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon3} alt="Big variety of candidates icon" />
							</div>
							<h3>Богат избор</h3>
							<p>В нашата платформа можете да откриете единствена по рода си селекция от оферти за работа, подбрани според вашите знания и умения.</p>
						</div>
					</div>

					<div className="col-lg-3 col-md-6">
						<div className="single-box">
							<div className="icon">
								<img src={icon4} alt="It's free icon" />
							</div>
							<h3>Безплатно е !</h3>
							<p>WorkFavour Ви предоставя възможност за допълнителен доход и кариерно развитие напълно безплатно.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}

export default OurFeatures
