type FieldType = "file" | "email" | "password" | "text" | "select" | "list" | "time" | "date" | "select-autocomplete" | "single-autocomplete";
export interface Field {
    name: string;
    type: FieldType;
    options?: string[];
    label?: string;
    placeholder?: string;
    helpText?: string;
    value?: any;
    removable?: boolean;
    disabled?: boolean;
}
