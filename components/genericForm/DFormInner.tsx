import * as React from "react";
// import {SyntheticEvent} from "react";
import DField from "./DField";
import AddDynamicField from "./AddDynamicField";
import DFormGroup from './DFormGroup';
import { Ref } from "./DForm";
import { Button, FormHelperText } from "@material-ui/core";

// import {ReactNode} from "react";

interface DFormProps {
    id: string,
    handleInputChange: (event: any, dynamic?: boolean) => void;
    onSubmit: (event: any) => void;
    hasErrors: boolean;
    errors: { [key: string]: any };
    fields: { [key: string]: any };
    fieldValues: { [key: string]: any };
    isSubmitting: boolean;
    remoteSubmit: boolean,
    forwardedRef?: any;
    dynamicAddition?: boolean;
    dynamicFields: { [key: string]: any };
    onDynamicAdd?: (key: string) => void
    onInputRemove?: (key: string) => void
}

class DFormInner extends React.Component<DFormProps> {
    constructor(props: DFormProps) {
        super(props);

        // this is dispatched here in order to not connect more components to state
        const { id } = this.props;
        const addFormToStore = undefined;
        if (addFormToStore) {
            addFormToStore(id)
        }
    }

    displayFields = (fields: { [key: string]: any }) => {
        const { id, fieldValues, handleInputChange, errors, onInputRemove } = this.props;

        return Object.keys(fields).map((name) => {
            const { validation, isRequired, initialValue, dynamicGen, removable, autocomplete, ...field } = fields[name];
            const { label, helpText, ...configProps } = field;

            const storeErrors = [];

            const errorsForField: string[] = [
                ...(errors[name] || []),
                ...(storeErrors[name] || [])
            ];

            const valueOfField = fieldValues[name] || "";

            const valid = errorsForField.length === 0;

            // fields
            return (
                <DFormGroup key={name}
                    name={name}
                    label={label}
                    errorsForField={errorsForField}
                    helpText={helpText}
                    removable={removable || dynamicGen}
                    onInputRemove={onInputRemove}
                >
                    <DField {...configProps}
                        value={valueOfField || ""}
                        onChange={handleInputChange}
                        dynamicGen={dynamicGen}
                        invalid={`${!valid}`}
                    />
                </DFormGroup>
            )
        });
    };

    // TODO: make constant
    displayGlobalErrors = () => {
        const { id } = this.props;
        const formsState = undefined;
        if (formsState === undefined || !formsState[id]
            || !formsState[id].globalErrors) {
            return;
        }

        const storeGlobalErors: string[] = formsState[id].globalErrors as string[] || [];

        return storeGlobalErors.map((error: string, i: number) => {
            return (
                <FormHelperText
                    key={error + i}>
                    {error}
                </FormHelperText>
            )
        })
    };

    render() {
        const { id } = this.props;
        const formsState = []
        if (!formsState[id]) {
            // action still not done
            return null;
        }

        const {
            fields, onSubmit, remoteSubmit, hasErrors, isSubmitting, forwardedRef,
            dynamicAddition, dynamicFields, onDynamicAdd,
        } = this.props;

        return (
            <form ref={forwardedRef} onSubmit={onSubmit} id={`dFrom_${id}`}>
                {this.displayGlobalErrors()}

                {this.displayFields(fields)}

                {dynamicAddition && Object.keys(dynamicFields).length > 0
                    && this.displayFields(dynamicFields)}

                {dynamicAddition && onDynamicAdd
                    && <AddDynamicField onDynamicAdd={onDynamicAdd} />}

                {!remoteSubmit && (<Button type="submit"
                    disabled={hasErrors || isSubmitting}>
                    {isSubmitting ? "Submitting..." : "Submit"}
                </Button>)}
            </form>
        );
    }
}

export default React.forwardRef<Ref, DFormProps>((props, ref) =>
    (<DFormInner {...props} forwardedRef={ref} />)
);
