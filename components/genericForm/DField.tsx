import * as React from 'react';
import { Field } from "./interface";
import { useState } from "react";
import { IconButton, Input, InputAdornment, List, ListItem, Grid, Paper, TextField } from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";

interface FieldProps extends Field {
    onChange: (event: any, dynamic?: boolean) => void;
    invalid: boolean;
    dynamicGen?: boolean,
    autoCompleteValues: string[]
}

class DFieldUnconnected extends React.Component<FieldProps> {
    render() {
        const { type } = this.props;
        switch (type) {
            case "file":
                return <FileField{...this.props} />
            case "date":
                return <DatePickerField {...this.props} />;
            case "time":
                return <TimePickerField {...this.props} />;
            case "single-autocomplete":
                return <SingleAutoCompleteField {...this.props} />;
            case "select-autocomplete":
                return <AutoCompleteSelectField {...this.props} />;
            case "password":
                return <PasswordField {...this.props} />;
            case "text":
            case "email":
            case "list":
            case "select":
                return <SelectField {...this.props} />;
            default:
                return <SelectField {...this.props} />;

        }
    }
}

export default DFieldUnconnected;

const SelectField: React.SFC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, ...restProps } = props;
    if (type === "list" || type === "select-autocomplete" || type === "single-autocomplete" || type === "file") {
        throw new Error("Invalid type for input field");
    }
    const safeValueForInput: "email" | "password" | "text" | "select" | "date" | "time" = type;

    return (
        <Input {...restProps} type={safeValueForInput} onChange={(event: any) => onChange(event, dynamicGen)}>
            {options && options.map((option: string, i: number) => {
                return <option key={option + i} value={option}>{option}</option>;
            })}
        </Input>
    )
};

const FileField: React.SFC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, value, ...restProps } = props;
    const onChangeWrapper = (event: any) => {
        event.persist();
        const fileList = event.target.files;
        const fileReader = new FileReader();
        if (fileReader && fileList && fileList.length) {
            fileReader.readAsArrayBuffer(fileList[0]);
            fileReader.onload = () => {
                const imageData = fileReader.result;
                const newEvent = {
                    target: {
                        name: event.target.name,
                        value: imageData
                    }
                };
                console.log("newEvent", newEvent);
                onChange(newEvent, dynamicGen);
            };
        }
    };
    return <Input {...restProps} type={"file"} onChange={(event) => onChangeWrapper(event)} />;
};

const PasswordField: React.SFC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, ...restProps } = props;

    const [showPassword, setShowPassword] = useState(false);

    return (
        <Input
            {...restProps}
            type={showPassword ? 'text' : 'password'}
            onChange={(event: any) => onChange(event, dynamicGen)}
            endAdornment={
                <InputAdornment position="end">
                    <IconButton
                        aria-label="Toggle password visibility"
                        onClick={() => setShowPassword(!showPassword)}
                    >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                </InputAdornment>
            }
        />
    )
};

const AutoCompleteSelectField: React.FC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, ...restProps } = props;

    const [selectedFields, setSelectedFields] = useState([] as any[]);
    const [filter, setFilter] = useState("");

    const updateFilter = (event: any) => {
        setFilter(event.target.value);
    }

    const advancedOnChange = (chosenValue: string, dynamicGen: boolean | undefined) => {
        console.log(selectedFields, chosenValue);
        setSelectedFields([...selectedFields, chosenValue])
        onChange({ target: { name: props.name, value: [...selectedFields, chosenValue] } }, dynamicGen);
    }

    const removeOnClick = (clickedItem: string) => {
        setSelectedFields(selectedFields.filter(sf => sf !== clickedItem));
    }

    const gridifyItem = (item: any, onClick: () => void) => {
        return <Grid item xs onClick={onClick}>
            <Paper>{item}</Paper>
        </Grid>
    }

    const generateSuggestedList = () => {
        if (!autoCompleteValues) {
            return [];
        }
        return autoCompleteValues.filter(value => {
            return value.startsWith(filter);
        }).map((value, i) => {
            if (i >= autoCompleteValues.length / 2) {
                return;
            }
            return gridifyItem(value, () => advancedOnChange(value, dynamicGen));
        })
    }

    const generateFullList = () => {
        if (!autoCompleteValues) {
            return [];
        }
        return autoCompleteValues.filter(value => {
            return value.startsWith(filter);
        }).map((value) => gridifyItem(value, () => advancedOnChange(value, dynamicGen)))
    };

    return (
        <div>
            <span>Selected</span>
            <Grid container spacing={8}>
                {
                    selectedFields.map(sf => gridifyItem(sf, () => removeOnClick(sf)))
                }
            </Grid>
            <Input
                {...restProps}
                type={'text'}
                onChange={(event: any) => updateFilter(event)}
                value={filter}
            />

            <span>Suggestions</span>
            <Grid container spacing={8}>
                {generateSuggestedList()}
            </Grid>

            <Grid container spacing={8}>
                {generateFullList()}
            </Grid>
        </div>
    );
};

const SingleAutoCompleteField: React.FC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, ...restProps } = props;

    const [filter, setFilter] = useState("");

    const updateFilter = (event: any) => {
        setFilter(event.target.value);
    };

    const advancedOnChange = (chosenValue: string, dynamicGen: boolean | undefined) => {
        setFilter(chosenValue);
        onChange({ target: { name: props.name, value: chosenValue } }, dynamicGen);
    };

    const gridifyItem = (item: any, onClick: () => void) => {
        return (
            <Grid key={item} item xs onClick={onClick}>
                <Paper>{item}</Paper>
            </Grid>
        );
    };

    const generateFullList = () => {
        if (!autoCompleteValues) {
            return [];
        }

        const noFoundPositionsMessage = `No positions found that start with ${filter}.`;
        const valuesMatchingFilter = autoCompleteValues.filter((value) => {
            return value.startsWith(filter);
        });

        if (valuesMatchingFilter.length === 0) {
            return [gridifyItem(noFoundPositionsMessage, console.log)];
        }

        return valuesMatchingFilter.map((value) => gridifyItem(value, () => advancedOnChange(value, dynamicGen)));
    };

    return (
        <div>
            <Input
                {...restProps}
                type={"text"}
                onChange={(event: any) => updateFilter(event)}
                value={filter}
            />

            <Grid container spacing={8}>
                {generateFullList()}
            </Grid>
        </div>
    );
};

const DatePickerField: React.FC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, placeholder, ...restProps } = props;
    return (
        <TextField
            {...restProps}
            type={type}
            onChange={(event: any) => onChange(event, dynamicGen)}
            InputLabelProps={{
                shrink: true,
            }}
        />
    );
};

const TimePickerField: React.FC<FieldProps> = (props) => {
    const { options, onChange, dynamicGen, type, autoCompleteValues, placeholder, ...restProps } = props;
    return (
        <TextField
            {...restProps}
            type={type}
            onChange={(event: any) => onChange(event, dynamicGen)}
            InputLabelProps={{
                shrink: true,
            }}
            inputProps={{
                step: 300, // 5 min
            }}
        />
    );
};

//
// const TextField: React.SFC<FieldProps> = (props) => {
//     const {options, ...restProps} = props;
//
//     return (
//         <Input {...restProps}/>
//     )
// }