import * as React from 'react';
import {ReactNode} from "react";
import {Button, FormControl, FormHelperText, Input, InputLabel} from "@material-ui/core";

interface DFGProps {
    name: string;
    label: string;
    children: ReactNode,
    errorsForField: string[],
    helpText: string;
    removable?: boolean;
    onInputRemove?: (key: string) => void;
}

class DFormGroup extends React.Component<DFGProps> {
    displayErrors = (currErrors: string[]) => {
        if (!currErrors) {
            return null;
        }

        return currErrors.map((error: string, i) => {
            return (
                <FormHelperText
                    key={error + i}>
                    {error}
                </FormHelperText>
            )
        })
    };

    render() {
        const {name, label, children, errorsForField, helpText, removable, onInputRemove} = this.props;

        // Dynamic
        if (removable && onInputRemove) {
            return (
                <FormControl>
                    <div style={{display: "inline-block", margin: "0 5px"}}>
                        <Button
                            onClick={() => onInputRemove(name)}>
                            {/* <Glyphicon glyph="remove-circle"/> */}
                        </Button>
                    </div>
                    <div style={{display: "inline-block", width: "75%"}}>
                        <InputLabel htmlFor="exampleEmail">{label}</InputLabel>
                        {children}
                    </div>

                    {this.displayErrors(errorsForField)}

                    {helpText !== undefined && <FormHelperText>{helpText}</FormHelperText>}

                </FormControl>
            )
        }

        return (
            <div>
                <FormControl error={errorsForField.length > 0}>
                    <InputLabel htmlFor={name}>{label}</InputLabel>
                    {children}
                    {this.displayErrors(errorsForField)}
                    {helpText !== undefined && <FormHelperText>{helpText}</FormHelperText>}
                </FormControl>
            </div>
        );
    }
}

export default DFormGroup;