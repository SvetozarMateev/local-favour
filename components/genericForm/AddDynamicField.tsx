import * as React from 'react';
import {Button, Input, InputLabel} from "@material-ui/core";
// import { Glyphicon } from "react-bootstrap";

interface DynamicFieldProps {
    onDynamicAdd: (key: string) => void
}

class DynamicFieldState {
    key: string = "";
}

class AddDynamicField extends React.Component<DynamicFieldProps, DynamicFieldState> {
    state = new DynamicFieldState();

    onChange = (event: any) => {
        const { value } = event.target;

        this.setState({
            key: value
        })
    };

    render() {
        return (
            <div>
                <div style={{ display: "inline-block", width: "35%" }}>
                    <InputLabel htmlFor="key">New Key</InputLabel>
                    <Input type="text"
                        name="key"
                        onChange={this.onChange} />
                </div>
                <div style={{ display: "inline-block", marginLeft: "5px" }}>
                    <Button 
                        onClick={() => this.props.onDynamicAdd(this.state.key)}>
                        {/* <Glyphicon glyph="plus-sign" /> */}
                    </Button>
                </div>
            </div>
        );
    }
}

export default AddDynamicField;